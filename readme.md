# Running unittests
unzip dummy.zip in root project dir
load module readfiles, and test.
Expected output:
```
Reloading module readfiles
Running tests for readfiles
| testing 0/6 Processing 10 newlines
Currently at 10.0 percent (1)
Duplicate Char Code: 0E+1 percent
Duplicate LOC Code: 0E+1 percent
Duplicate code rank was "++"
Processing 23460 newlines
Currently at 0.00426257459500 percent (1)
...
Currently at 99.9445865300 percent (23447)
Duplicate Char Code: 4.92032297000 percent
Duplicate LOC Code: 5.34526854200 percent
Duplicate code rank was "o"
Test report for readfiles                                                   
        all 6/6 tests succeeded
bool: true
```

# Example output
For smallSQL we get:
```rascal
rascal>main(|project://smallsql0.21_src|);
Reloading module readfiles
Generating ASTs - ok

--- Halstead metrics ---
Volume: 2553082.3099931157 Difficulty: 5000.26399488 Effort: 12766085550.623635255380047616 TimeToProgramHour: 197007.49306517955641018592 Bugs: 851.0274366643719

--- Maintainability index ---
Code contains 3864 branches and 2202 methods, resulting in an estimated complexity of 2.754768392
Cyclomatic Complexity: 2.754768392
Lines of code: 29351
Maintainability Index: -72.998945512370

--- SIG KLOC ---
29351 resulted in a rank of ++

--- SIG Complexity per unit ---
Complexity relative to LOC was rated as: ("+":51.68818779778900,"-":9.0048039248500,"o":7.2195155190200,"++":0.0,"--":4.933392388700), resulting in a risk of: "-"
Unit size relative to LOC was rated as: ("+":23.46427719843900,"-":20.4967462781700,"o":11.6588872608500,"++":0.0,"--":17.225988892900), resulting in a risk of: "--"
Complexity per unit resulted in a rank of -
Size per unit resulted in a rank of --

--- SIG Duplication ---
Processing 23460 newlines
Currently at 0.00426257459500 percent (1)
Currently at 5.26427962500 percent (1235)
Currently at 10.5242966800 percent (2469)
Currently at 15.7843137300 percent (3703)
Currently at 21.0443307800 percent (4937)
Currently at 26.3043478300 percent (6171)
Currently at 31.5643648800 percent (7405)
Currently at 36.8243819300 percent (8639)
Currently at 42.0843989800 percent (9873)
Currently at 47.3444160300 percent (11107)
Currently at 52.6044330800 percent (12341)
Currently at 57.8644501300 percent (13575)
Currently at 63.1244671800 percent (14809)
Currently at 68.3844842300 percent (16043)
Currently at 73.6445012800 percent (17277)
Currently at 78.9045183300 percent (18511)
Currently at 84.1645353800 percent (19745)
Currently at 89.4245524300 percent (20979)
Currently at 94.6845694800 percent (22213)
Currently at 99.9445865300 percent (23447)
Duplicate Char Code: 4.92032297000 percent
Duplicate LOC Code: 5.34526854200 percent
Duplicate code rank was "o"
Duplicated code resulted in a rank of o

--- SIG Unit testing ---
Coverage 81.80981908733700

--- SIG Maintainability ---
                VOL     COM     DUP     SIZ     TST     TOTAL
                ++      -       o       --      +
analyseability  X               X       X       X       o
changeability           X       X                       -
stability                                       X       +
testability             X               X       X       -
```

For HSQLDB we get:
```rascal
rascal>main(|project://hsqldb-2.3.1|);
Generating ASTs - ok

--- Halstead metrics ---
Volume: 19100136.23392454925 Difficulty: 18421.519090625 Effort: 351853524266.77937477472302578125 TimeToProgramHour: 5429838.3374502989934370837311922 Bugs: 6366.71207797484975

--- Maintainability index ---
Code contains 26597 branches and 10059 methods, resulting in an estimated complexity of 3.644099811
Cyclomatic Complexity: 3.644099811
Lines of code: 166845
Maintainability Index: -111.819306654950

--- SIG KLOC ---
166845 resulted in a rank of +

--- SIG Complexity per unit ---
Complexity relative to LOC was rated as: ("+":53.909317032309600,"-":10.8885492522400,"o":15.42389643068800,"++":0.0,"--":9.0173514338000), resulting in a risk of: "--"
Unit size relative to LOC was rated as: ("+":16.118553149324600,"-":23.6321136380400,"o":16.57046959814300,"++":0.0,"--":32.9179777635300), resulting in a risk of: "--"
Complexity per unit resulted in a rank of --
Size per unit resulted in a rank of --

--- SIG Duplication ---
Processing 163488 newlines
Currently at 0.000611665688000 percent (1)
Currently at 0.755407124700 percent (1235)
Currently at 1.51020258400 percent (2469)
Currently at 2.26499804300 percent (3703)
Currently at 3.01979350200 percent (4937)
Currently at 3.77458896100 percent (6171)
Currently at 4.52938442000 percent (7405)
Currently at 5.28417987900 percent (8639)
Currently at 6.03897533800 percent (9873)
Currently at 6.79377079700 percent (11107)
Currently at 7.54856625600 percent (12341)
Currently at 8.30336171500 percent (13575)
Currently at 9.05815717400 percent (14809)
Currently at 9.81295263300 percent (16043)
Currently at 10.5677480900 percent (17277)
Currently at 11.3225435500 percent (18511)
Currently at 12.0773390100 percent (19745)
Currently at 12.8321344700 percent (20979)
Currently at 13.5869299300 percent (22213)
Currently at 14.3417253900 percent (23447)
Currently at 15.0965208500 percent (24681)
Currently at 15.8513163000 percent (25915)
Currently at 16.6061117600 percent (27149)
Currently at 17.3609072200 percent (28383)
Currently at 18.1157026800 percent (29617)
Currently at 18.8704981400 percent (30851)
Currently at 19.6252936000 percent (32085)
Currently at 20.3800890600 percent (33319)
Currently at 21.1348845200 percent (34553)
Currently at 21.8896799800 percent (35787)
Currently at 22.6444754400 percent (37021)
Currently at 23.3992708900 percent (38255)
Currently at 24.1540663500 percent (39489)
Currently at 24.9088618100 percent (40723)
Currently at 25.6636572700 percent (41957)
Currently at 26.4184527300 percent (43191)
Currently at 27.1732481900 percent (44425)
Currently at 27.9280436500 percent (45659)
Currently at 28.6828391100 percent (46893)
Currently at 29.4376345700 percent (48127)
Currently at 30.1924300300 percent (49361)
Currently at 30.9472254800 percent (50595)
Currently at 31.7020209400 percent (51829)
Currently at 32.4568164000 percent (53063)
Currently at 33.2116118600 percent (54297)
Currently at 33.9664073200 percent (55531)
Currently at 34.7212027800 percent (56765)
Currently at 35.4759982400 percent (57999)
Currently at 36.2307937000 percent (59233)
Currently at 36.9855891600 percent (60467)
Currently at 37.7403846200 percent (61701)
Currently at 38.4951800700 percent (62935)
Currently at 39.2499755300 percent (64169)
Currently at 40.0047709900 percent (65403)
Currently at 40.7595664500 percent (66637)
Currently at 41.5143619100 percent (67871)
Currently at 42.2691573700 percent (69105)
Currently at 43.0239528300 percent (70339)
Currently at 43.7787482900 percent (71573)
Currently at 44.5335437500 percent (72807)
Currently at 45.2883392100 percent (74041)
Currently at 46.0431346600 percent (75275)
Currently at 46.7979301200 percent (76509)
Currently at 47.5527255800 percent (77743)
Currently at 48.3075210400 percent (78977)
Currently at 49.0623165000 percent (80211)
Currently at 49.8171119600 percent (81445)
Currently at 50.5719074200 percent (82679)
Currently at 51.3267028800 percent (83913)
Currently at 52.0814983400 percent (85147)
Currently at 52.8362938000 percent (86381)
Currently at 53.5910892500 percent (87615)
Currently at 54.3458847100 percent (88849)
Currently at 55.1006801700 percent (90083)
Currently at 55.8554756300 percent (91317)
Currently at 56.6102710900 percent (92551)
Currently at 57.3650665500 percent (93785)
Currently at 58.1198620100 percent (95019)
Currently at 58.8746574700 percent (96253)
Currently at 59.6294529300 percent (97487)
Currently at 60.3842483900 percent (98721)
Currently at 61.1390438400 percent (99955)
Currently at 61.8938393000 percent (101189)
Currently at 62.6486347600 percent (102423)
Currently at 63.4034302200 percent (103657)
Currently at 64.1582256800 percent (104891)
Currently at 64.9130211400 percent (106125)
Currently at 65.6678166000 percent (107359)
Currently at 66.4226120600 percent (108593)
Currently at 67.1774075200 percent (109827)
Currently at 67.9322029800 percent (111061)
Currently at 68.6869984300 percent (112295)
Currently at 69.4417938900 percent (113529)
Currently at 70.1965893500 percent (114763)
Currently at 70.9513848100 percent (115997)
Currently at 71.7061802700 percent (117231)
Currently at 72.4609757300 percent (118465)
Currently at 73.2157711900 percent (119699)
Currently at 73.9705666500 percent (120933)
Currently at 74.7253621100 percent (122167)
Currently at 75.4801575700 percent (123401)
Currently at 76.2349530200 percent (124635)
Currently at 76.9897484800 percent (125869)
Currently at 77.7445439400 percent (127103)
Currently at 78.4993394000 percent (128337)
Currently at 79.2541348600 percent (129571)
Currently at 80.0089303200 percent (130805)
Currently at 80.7637257800 percent (132039)
Currently at 81.5185212400 percent (133273)
Currently at 82.2733167000 percent (134507)
Currently at 83.0281121600 percent (135741)
Currently at 83.7829076100 percent (136975)
Currently at 84.5377030700 percent (138209)
Currently at 85.2924985300 percent (139443)
Currently at 86.0472939900 percent (140677)
Currently at 86.8020894500 percent (141911)
Currently at 87.5568849100 percent (143145)
Currently at 88.3116803700 percent (144379)
Currently at 89.0664758300 percent (145613)
Currently at 89.8212712900 percent (146847)
Currently at 90.5760667400 percent (148081)
Currently at 91.3308622000 percent (149315)
Currently at 92.0856576600 percent (150549)
Currently at 92.8404531200 percent (151783)
Currently at 93.5952485800 percent (153017)
Currently at 94.3500440400 percent (154251)
Currently at 95.1048395000 percent (155485)
Currently at 95.8596349600 percent (156719)
Currently at 96.6144304200 percent (157953)
Currently at 97.3692258800 percent (159187)
Currently at 98.1240213300 percent (160421)
Currently at 98.8788167900 percent (161655)
Currently at 99.6336122500 percent (162889)
Duplicate Char Code: 9.16222050800 percent
Duplicate LOC Code: 9.30527011200 percent
Duplicate code rank was "o"
Duplicated code resulted in a rank of o

--- SIG Unit testing ---
Coverage 44.953100183069800

--- SIG Maintainability ---
                VOL     COM     DUP     SIZ     TST     TOTAL
                +       --      o       --      -
analyseability  X               X       X       X       -
changeability           X       X                       -
stability                                       X       -
testability             X               X       X       --
```
