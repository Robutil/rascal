module example

import readfiles;

import IO;
import Set;
import List;
import Map;
import Node;
import String;
import util::Math;
import lang::java::m3::Core;
import lang::java::m3::AST;
import lang::java::jdt::m3::Core;
import lang::java::jdt::m3::AST;

list[Declaration] getASTs(loc projectLocation){
	M3 model = createM3FromEclipseProject(projectLocation);
	list[Declaration] asts = [];
	for (m <- model.containment, m[0].scheme == "java+compilationUnit"){
		asts += createAstFromFile(m[0], true);
	}
	return asts;
}

list[Declaration] getSqlAst(){
	return getASTs(|project://smallsql0.21_src|);
}

tuple[int, int] getOperandStatistics(list[Declaration] asts){
	map[str,str] used = ();
	int totalOperands = 0;
	visit(asts){
		case \characterLiteral(str charValue): {
			used[charValue] = "characterLiteral";
			totalOperands += 1;
		}
		case \number(str numberValue): {
			used[numberValue] = "number";
			totalOperands += 1;
		}
		case \booleanLiteral(bool boolValue): {
			if (boolValue) {
				used["true"] = "booleanLiteral";
			} else {
				used["false"] = "booleanLiteral";
			}
			totalOperands += 1;
		}
		case \stringLiteral(str stringValue): {
			used[stringValue] = "stringLiteral";
			totalOperands += 1;
		}
		case \variable(str name, _): {
			used[name] = "variable";
			totalOperands += 1;
		}
		case \variable(str name, _, _): {
			used[name] = "variable";
			totalOperands += 1;
		}
		case \simpleName(str name): {
			used[name] = "simpleName";
			totalOperands += 1;
		}
		case \parameter(Type \type, str name, int extraDimensions): {
			used[name] = "parameter";
			totalOperands += 1;
		}
		case \vararg(Type \type, str name): {
			used[name] = "vararg";
			totalOperands += 1;
		}
	}
	return <totalOperands, size(used)>;
}

tuple[int totalOperands, int uniqueOperands, int totalOperators, int uniqueOperators] getHalsteadNumbers(list[Declaration] asts){
	tuple[int, int] operandStatistics = getOperandStatistics(asts);
	int totalOperands = operandStatistics[0];
	int uniqueOperands = operandStatistics[1];
	
	map[str, str] used = ();
	int totalOperators = 0;
	visit(asts){
		case node x: {
			if (arity(x) > 0) {
				// Not a simple node
				if (str y := getChildren(x)[0]) {
					used[y] = ""; // We only care about types that we can read
				} //else -> Do we count suger nodes? 
			} else {
				used[getName(x)] = "";
			}
			
			totalOperators += 1;
		}
	}
	
	totalOperators -= totalOperands; // We counted all nodes, so substract operands
	int uniqueOperators = size(used) - uniqueOperands;
	return <totalOperands, uniqueOperands, totalOperators, uniqueOperators>;
}

real printHalsteadMetrics(tuple[int totalOperands, int uniqueOperands, int totalOperators, int uniqueOperators] numbers){
	real vocabulary = numbers.uniqueOperands + numbers.uniqueOperators + 0.0;
	int programLength = numbers.totalOperands + numbers.totalOperators;
	
	real volume = programLength * log(vocabulary, 2);
	real difficulty = (numbers.uniqueOperators / 2.0) * (numbers.totalOperands / (numbers.uniqueOperands + 0.0));
	real effort = difficulty * volume;
	real timeToProgramSec = effort / 18.0;
	real hours = timeToProgramSec / 3600.0;
	real bugs = volume / 3000.0;
	
	// For smallSQL we get:
	//     Volume: 2553082 Difficulty: 5000 Effort: 12766085550 TimeToProgramHour: 197007 Bugs: 851
	// Approximately 23 years of development. Given that the project was started somewhere before 2004, and finished in 2011 this actually seems quite accurate. It then
	// depicts 7 years of development, but with a team of 4 people.
	println("Volume: <volume> Difficulty: <difficulty> Effort: <effort> TimeToProgramHour: <hours> Bugs: <bugs>");
	return volume;
}

int getSimpleCyclomaticComplexity(list[Declaration] asts) {
	// Very cheap way to get an impression of Cyclomatic Complexity
	// We should count connected methods, etc, but in order to save time
	// we can see how many conditions the code has, and how many methods.
	// The average of those, plus one, gives a good inidication of the
	// complexity.
	int branches = 1;
	visit (asts) {
		case \if(_, _): branches += 1;
		case \if(_, _, _): branches += 1;
		case \conditional(_, _, _): branches += 1;
		case \do(_, _): branches += 1;
		case \foreach(_, _, _): branches += 1;
		case \for(_, _, _, _): branches += 1;
		case \for(_, _, _): branches += 1;		
		case \while(_, _): branches += 1;
		case \catch(_, _): branches += 1;
		case \case(_): branches += 1;
		case \conditional(_,_,_): branches += 1;
		case \defaultCase(): branches += 1;
		case \infix(lhs, operator, rhs): {
			println("<operator>");
		}
	}
	return branches;
}

tuple[int linesOfCode, int complexity] snapFunc(Declaration func) {
	str source = readFile(func.src);
	int linesOfCode = getLinesOfCode(func.src);
	return <linesOfCode, getSimpleCyclomaticComplexity([func])>;
}

str riskToRank(int risk) {
	// ++ 0
	// +  1
	// o  2
	// -  3
	// -- 4
	switch(risk) {
		case 0: return "++";
		case 1: return "+";
		case 2: return "o";
		case 3: return "-";
		default: return "--";
	}
}

int rankToRisk(str rank) {
	switch(rank) {
		case "++": return 0;
		case "+": return 1;
		case "o": return 2;
		case "-": return 3;
		default: return 4;
	}
}

str ccToRisk(int cc) {
	// For Java projects only   // Risk Evaluations
	if (cc < 10) return "+";    // simple, without much risk
	if (cc < 20) return "o";    // more complex, moderate risk
	if (cc < 50) return "-";    //complex, high risk
	return "--";                //    untestable, very high risk
}

str coverageToRisk(real coverage) {
	if (coverage > 95.0) return "++";
	if (coverage > 80.0) return "+";
	if (coverage > 60.0) return "o";
	if (coverage > 20.0) return "-";
	return "--";        
}

str unitSizeToRisk(int unitComplexity) {
	return ccToRisk(unitComplexity);
}

/*
 * Expects a schema like so:
 * rank  moderate  high  very_high
   ++    25        0     0
   +     30        5     0
   o     40        10    0
   -     50        15    5
   --    100       100   100
*/
int riskByGradeSchema(list[list[real]] schema, list[real] grades){
	int risk = 0; // ++, default (see riskToRank for levels)
	
	for(int y <- [0..3]) {
		for(int x <- [0..6]) {
			if (grades[y] < schema[x][y]) {
				if (x > risk) {
					risk = x;
				}
				break;
			}
		}
	}
	return risk;
}

str rankMYbyUnitTest(list[Declaration] asts, map[str, tuple[real, int]] methodToSizeAndComplexity){
	// We don't check \assert statements because it may give a bad indication. For example, with JUNIT
	// we should also check stuff like assertNotSame, assertNotNull, assertTrue, etc.
	map[str, int] coveredMethods = ();
	
	visit (asts) {
		case a:\method(_, unittestname, _, _, _): {
			if (contains(a.src.path, "Test")) {
				//println("Found unittest <unittestname> in <a.src.path>"); 
				visit (a) {
				    case \methodCall(_, name, _): coveredMethods[name]?0 += 1;
	    			case \methodCall(_, _, name, _): coveredMethods[name]?0 += 1;
				}
			}
		}
	}
	
	real coverage = 0.0;
	for (method <- coveredMethods) {
		if (method in methodToSizeAndComplexity) {
			coverage += (methodToSizeAndComplexity[method][0] * max(1, (coveredMethods[method] - methodToSizeAndComplexity[method][1])));
			// This coverage checking is very, very crude
			// It does not take into account that methods can call other methods, it assumes
			// each method has a unit test. The thought behind it is that when methods call 
			// other methods it increases the complexity. This is quite conservative then when
			// estimating coverage. To balance this we assume that the tested method is fully
			// tested if a unit test calls it. In the future we can use:
			//     methodToSizeAndComplexity[methodName][1] -> complexity for a function
			//     coveredMethods[methodName]               -> how many times a function is called from a test
			// It easily gets more complex though, if you take into account for loops, recursion, etc.
			// Hence the idea is shown here, but in practise we just use this "easy" test.
			// To provide a bonus in coverage we allow methods that are tested many times to be duplicated in the loc they cover. However, this bonus
			// is tied to the Cyclomatic Complexity.
		} 
	}
	
	println("Coverage <min(coverage, 100.0)>");
	return coverageToRisk(min(coverage, 100.0));
}

tuple[str, str, map[str, tuple[real, int]]] rankMYbyUnitComplexity(list[Declaration] asts, int totalLinesOfCode) {
	map[str, real] ccRelLoc = ("++": 0.0, "+": 0.0, "o": 0.0, "-": 0.0, "--": 0.0);
	map[str, real] unitSize = ("++": 0.0, "+": 0.0, "o": 0.0, "-": 0.0, "--": 0.0);
	map[str, tuple[real, int]] methodToSizeAndComplexity = ();
	
	visit (asts) {
		case a:\method(_, name, _, _, _): {
			int linesOfCode = getLinesOfCode(a.src);
			int branches = 1;
			visit (a) {
				case \if(_, _): branches += 1;
				case \if(_, _, _): branches += 1;
				case \conditional(_, _, _): branches += 1;
				case \do(_, _): branches += 1;
				case \foreach(_, _, _): branches += 1;
				case \for(_, _, _, _): branches += 1;
				case \for(_, _, _): branches += 1;		
				case \while(_, _): branches += 1;
				case \catch(_, _): branches += 1;
				case \case(_): branches += 1;
				case \defaultCase(): branches += 1;
				case \infix(lhs, operator, rhs): {
					if (operator == "&&" || operator == "||") {
						branches += 1;
					}
				}
			}
			real percentageMethod = linesOfCode / (0.0 + totalLinesOfCode) * 100;
			ccRelLoc[ccToRisk(branches)] += percentageMethod;
			unitSize[unitSizeToRisk(linesOfCode)] += percentageMethod;
			
			methodToSizeAndComplexity[name] = <percentageMethod, branches>;
		}
	}
	
	list[real] unitComplexityGrades = [ccRelLoc["o"], ccRelLoc["-"], ccRelLoc["--"]];
	int riskForUnitComplexity = riskByGradeSchema(GRADING_SCHEMA_COMPLEXITY_REL_UNIT, unitComplexityGrades);
	str rankForUnitComplexity = riskToRank(riskForUnitComplexity);
	println("Complexity relative to LOC was rated as: <ccRelLoc>, resulting in a risk of: \"<rankForUnitComplexity>\"");
		
	list[real] unitSizeGrades = [unitSize["o"], unitSize["-"], unitSize["--"]];
	int riskForUnitSize = riskByGradeSchema(GRADING_SCHEMA_COMPLEXITY_REL_UNIT, unitSizeGrades);
	str rankForUnitSize = riskToRank(riskForUnitSize);
	println("Unit size relative to LOC was rated as: <unitSize>, resulting in a risk of: \"<rankForUnitSize>\"");
	
	return <rankForUnitComplexity, rankForUnitSize, methodToSizeAndComplexity>;
}

real getCyclomaticComplexity(list[Declaration] asts) {
	// Very cheap way to get an impression of Cyclomatic Complexity
	// We should count connected methods, etc, but in order to save time
	// we can see how many conditions the code has, and how many methods.
	// The average of those, plus one, gives a good inidication of the
	// complexity.
	int branches = 1;
	int methods = 1;
	visit (asts) {
		case \if(_, _): branches += 1;
		case \if(_, _, _): branches += 1;
		case \conditional(_, _, _): branches += 1;
		case \do(_, _): branches += 1;
		case \foreach(_, _, _): branches += 1;
		case \for(_, _, _, _): branches += 1;
		case \for(_, _, _): branches += 1;		
		case \while(_, _): branches += 1;
		case \catch(_, _): branches += 1;
		case \case(_): branches += 1;
		case \defaultCase(): branches += 1;
		case a:\method(_, _, _, _, _): {
		methods += 1;}
		//case \method(_, _, _, _): methods += 1;
	}
	
	real estimatedCyclicComplexity = 1.0 + branches / (methods + 0.0);
	println("Code contains <branches> branches and <methods> methods, resulting in an estimated complexity of <estimatedCyclicComplexity>");
	// For smallSQl we get:
	//    Code contains 3864 branches and 4529 methods, resulting in an estimated complexity of 1.8531684699
	// This value is low considering the size of the project. However, it is also a bit tricky because a very
	// large portion of those methods are getters and setters. 
	return estimatedCyclicComplexity;
}

real getMaintainabilityIndex(real volume, real cc, int linesOfCode){
	// Doesn't really work well for large projects. E.g. smallSql
	// It is also extremely dependant on how you scope things. In this case
	// we take the entire project, but just by this measure the loc already
	// cause a drastically low MI, let alone with the volume and CyclicComplexity.
	//     rascal> getMaintainabilityIndex(2553082.0, 2.0, 29856);
	//     real: -73.101707310914
	return 171.0 - 5.2 * log(volume, E()) - 0.23 * cc - 16.2 * log(linesOfCode, E());
}

str rankMYbyLinesOfCode(int linesOfCode){
	// For Java projects only              // MAN YEARS
	if (linesOfCode < 66000) return "++";  // 0  - 8
	if (linesOfCode < 246000) return "+";  // 8  - 30
	if (linesOfCode < 665000) return "o";  // 30 - 80
	if (linesOfCode < 1310000) return "-"; // 80 - 160 
	return "--";                           //    > 160
}

void main(loc path) {
	print("Generating ASTs");
	list[Declaration] asts = getASTs(path);
	println(" - ok");
	
	println("\n--- Halstead metrics ---");
	halsteadNumbers = getHalsteadNumbers(asts);
	real halsteadVolume = printHalsteadMetrics(halsteadNumbers);
	
	println("\n--- Maintainability index ---");
	real cc = getCyclomaticComplexity(asts);
	int linesOfCode = getLinesOfCode(path);
	real mi = getMaintainabilityIndex(halsteadVolume, cc, linesOfCode);
	println("Cyclomatic Complexity: <cc>");
	println("Lines of code: <linesOfCode>");
	println("Maintainability Index: <mi>");
	
	println("\n--- SIG KLOC ---");
	str sigKloc = rankMYbyLinesOfCode(linesOfCode);
	println("<linesOfCode> resulted in a rank of <sigKloc>");
	
	println("\n--- SIG Complexity per unit ---");
	unit = rankMYbyUnitComplexity(asts, linesOfCode);
	str sigComplexity = unit[0];
	str sigUnitSize = unit[1];
	println("Complexity per unit resulted in a rank of <sigComplexity>");
	println("Size per unit resulted in a rank of <sigUnitSize>");
	
	println("\n--- SIG Duplication ---");
	str sigDuplicate = rankMYbyDuplicateCode(path);
	println("Duplicated code resulted in a rank of <sigDuplicate>");
	
	println("\n--- SIG Unit testing ---");
	str sigUnittest = rankMYbyUnitTest(asts, unit[2]);
	
	println("\n--- SIG Maintainability ---");
	int sigRankVolume = rankToRisk(sigKloc);
	int sigRankComplexity = rankToRisk(sigComplexity);
	int sigRankDuplicates = rankToRisk(sigDuplicate);
	int sigRankUnitsize = rankToRisk(sigUnitSize);
	int sigRankUnittest = rankToRisk(sigUnittest);
	
	int sigRankAnalyse = round((sigRankVolume + sigRankDuplicates + sigRankUnitsize + sigRankUnittest) / 4.0);
	int sigRankChange = round((sigRankComplexity + sigRankDuplicates) / 2.0);
	int sigRankStability = sigRankUnittest;
	int sigRankTestability = round((sigRankComplexity + sigRankUnitsize + sigRankUnittest) / 3.0);
	println("                VOL\tCOM\tDUP\tSIZ\tTST\tTOTAL");
	println("                <sigKloc> \t<sigComplexity> \t<sigDuplicate> \t<sigUnitSize> \t<sigUnittest>");
	println("analyseability  X  \t    \tX  \tX  \tX  \t<riskToRank(sigRankAnalyse)>");
	println("changeability      \tX   \tX  \t   \t   \t<riskToRank(sigRankChange)>");
	println("stability          \t    \t   \t   \tX  \t<riskToRank(sigRankStability)>");
	println("testability        \tX   \t   \tX  \tX  \t<riskToRank(sigRankTestability)>");

}

//public list[Declaration] sqlAST = getSqlAst();
public list[list[real]] GRADING_SCHEMA_COMPLEXITY_REL_UNIT = [
	[25.0, 1.0, 1.0],
	[30.0, 5.0, 1.0],
	[40.0, 10.0, 0.0],
	[50.0, 15.0, 5.0],
	[100.0, 100.0, 100.0]
];
