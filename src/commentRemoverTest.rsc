module commentRemoverTest

import readfiles;
import IO;
import String;

str comment = "This should be a /* bla bla bla bla */ regular sentence";

void main() {
	println(stripBetweenWithDepth("This should be a /* bla bla bla bla */ regular sentence","/*","*/"));
	println(stripBetweenWithDepth("This should be a /* bla bla /* bla bla bla bla */  bla bla */ regular sentence","/*","*/"));
	println(stripBetweenWithDepth("This should be a /* bla /* bla bla bla bla */  bla bla bla */ regular sentence","/*","*/"));
	println(stripBetweenWithDepth("This should be a /* bla bla bla /* bla bla /* bla /* bla bla bla bla */  bla  /* bla bla bla bla */ bla bla */  bla bla */  bla */ regular/*  /* bla bla bla bla */ bla bla bla bla */ sentence","/*","*/"));
}