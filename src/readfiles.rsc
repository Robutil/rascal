module readfiles

import IO;
import Set;
import List;
import String;
import util::Math;
import lang::java::m3::Core;
import lang::java::m3::AST;
import lang::java::jdt::m3::Core;
import lang::java::jdt::m3::AST;

/*
 * Gets locations of all Java files specified in the root project.
 * Usage:
 *   rascal> getJavaFiles(|project://dummy|);
 *   list[loc]: [|project://dummy/src/main.java|]
 */
list[loc] getJavaFiles(loc path) {
	list[loc] javaFiles = [];
	
	for (item <- listEntries(path)) {
		loc currentPath = path + ("/" + item);
		
		if (isDirectory(currentPath)) {
			javaFiles += getJavaFiles(currentPath);
		} else if (endsWith(currentPath.path, ".java")) {
			javaFiles += currentPath;
		}
	}
	return javaFiles;
}

/*
 * Removes everything that matches that pattern [blockStart*blockEnd], this includes
 * blockStart and BlockEnd. Can be used to strip single line and block comments.
 */
str stripBetween(str source, str blockStart, str blockEnd) {	
	int blockOpening = findFirst(source, blockStart);
	if (blockOpening == -1) return source; // no blockStart found, we are done
	
	// Extract all code till start of blockEnd
	str out = substring(source, 0, blockOpening);
	str remainder = substring(source, blockOpening + 1); 
	// TODO: nesting
	
	int blockClosing = findFirst(remainder, blockEnd);
	if (blockClosing == -1) return source; // We cannot find blockEnd, just abort
	
	remainder = substring(remainder, blockClosing + 2); // Strip away everything
	out += stripBetween(remainder, blockStart, blockEnd); // Search for additional blocks in remainder
	
	return out;
}

str stripBetweenWithDepth(str source, str blockStart, str blockEnd) {
	int blockOpening = findFirst(source, blockStart);
	
	if (blockOpening == -1) {
		//println("\n --- no comments to remove --- ");
		return source;
	}
	
	int tokenLength = size(blockStart);
	str remainder = source;
	int marker = 0;
	int commentDepth = -1;
	str out = "";
	
	while( size(remainder) > 0 ) {
		if (commentDepth == -1) {
			//we are not in comments, look only for a start
			marker = findFirst(remainder,blockStart);
			
			if (marker == -1) {
				//no markers found, text is clear
				
				out += remainder;
				return out;
			}
			else {
				//a marker is found, all text untill the marker is good
				//println("\n we find an start, depth increasing");
				commentDepth += 1;
				out += substring(remainder,0,marker);
				
			}
			
		} 
		else {
			int startMarker = findFirst(remainder,blockStart);
			int endMarker = findFirst(remainder,blockEnd);
			if (endMarker == -1) {
				//this can't be, we expect at least one comment end token
				//println("\n ERROR: expected at least one end of comment block token");
				//return source;
				return out;
			}
			if ((startMarker == -1) || (startMarker > endMarker)) {
				//we dont see a start, or we see an end sooner
				//println("\n we find an end, depth decreasing");
				marker = endMarker;
				commentDepth -= 1;
			}
			else {
				//a startMarker is found and, faster than an endMarker
				commentDepth += 1;
				marker = startMarker;
				//println("\n we find an start, depth increasing");
			}
			//out += substring(remainder,0,marker);s
			//remainder = substring(remainder,marker+1);
			
		}
		remainder = substring(remainder,min(marker+tokenLength,size(remainder)-1));
		//marker = min(findFirst(remainder, blockStart),findFirst(remainder, blockEnd)); 
		//take the first block token that's either a start or end
		
		//remainder = substring(remainder,marker);	
	}
	return out;
	//// no comments, so done
	//int nextOpening = blockOpening;
	//str out = substring(source, 0, blockOpening);
	//str remainder = substring(source, blockOpening + 1); 
	//while (nextOpening != -1) {
	//	nextOpening = findFirst(remainder, blockStart
	//}
	//
	//int blockClosing = findFirst(remainder, blockEnd);
	//if (blockClosing == -1) return source; // We cannot find blockEnd, just abort
	
	
	
}

/*
 * Removes block comments and single line comments from a given string.
 */
str removeComments(str source) {
	return stripBetweenWithDepth(stripBetweenWithDepth(source, "/*", "*/"), "//", "\n");
	//return stripBetweenWithDepth(stripBetweenWithDepth(source, "//", "\n"), "/*", "*/");	
}

/*
 * Strips away all empty lines and returns a list of non-empty code lines.
 */
list[str] getRawCode(str source) {
	list[str] rawCode = [];
	for (line <- split("\n", source)) {
		if(!isEmpty(line)) {
			rawCode += line;
		}
	}
	return rawCode;
}

int getLinesOfCode(loc path) {
	if (isDirectory(path)) {
		return getLinesOfCode(getJavaFiles(path));
	}
	
	str source = readFile(path);
	str out = removeComments(source);
	
	list[str] rawCode = getRawCode(out);
	
	return size(rawCode);
}

int getLinesOfCode(list[loc] paths) {
	totalLinesOfCode = 0;
	for (file <- paths) {
		totalLinesOfCode += getLinesOfCode(file);
	}
	return totalLinesOfCode;
}

tuple[real, real] calculateDuplicateCode(str source) {
	int nLines = 7;
	map[str, int] duplicates = ();
	list[int] newlines = findAll(source, "\n");
	println("Processing <size(newlines)> newlines");
	
	int totalDuplicateCode = 0;
	int totalDuplicateCodeLoc = 0;
	
	int i = 0;
	int previousChunkSize = -1;
	while( (i+nLines) < size(newlines)) {
		if (i % 1234 == 1) {
			real remainder = i / (size(newlines) + 0.0) * 100;
			println("Currently at <remainder> percent (<i>)");
		}
		
		int startIndex = newlines[i];
		int endIndex = newlines[i+nLines];
		str chunk = source[startIndex..endIndex];
		duplicates[chunk]?0 += 1;
		
		if (duplicates[chunk] > 1) {
			if (previousChunkSize != -1) {
				// Previous chunk was also duplicate code
				totalDuplicateCode += (size(chunk) - previousChunkSize); // Length of current chunk, minus length of previous chunk without the first line (our overlap)
				totalDuplicateCodeLoc += 1;
				
			} else {
				totalDuplicateCode += size(chunk);
				totalDuplicateCodeLoc += (nLines - 1);
			}
			previousChunkSize = size(chunk) - (newlines[i+1] - newlines[i]); // Length of chunk, minus the first line
		} else {
			previousChunkSize = -1; // We did not hit any duplicate code
		}
		
		i+=1;
	}
	real percentageDuplicateCode = totalDuplicateCode / (0.0 + size(source)) * 100;
	real percentageDuplicateCodeLoc= totalDuplicateCodeLoc / (0.0 + size(newlines)) * 100;
	println("Duplicate Char Code: <percentageDuplicateCode> percent");
	println("Duplicate LOC Code: <percentageDuplicateCodeLoc> percent");
	return <percentageDuplicateCode, percentageDuplicateCodeLoc>;
}

str rankDuplicateCodePercentage(real percentage) {
	if (percentage > 20.0) return "--";
	if (percentage > 10.0) return "-";
	if (percentage > 5.0) return "o"; 
	if (percentage > 3.0) return "+";
	return "++";
}

str rankMYbyDuplicateCode(loc project){
	list[loc] files = getJavaFiles(project);
	// We dont care about where the duplicate code is, only how many there is
	str allCode = "";
	for (file <- files) {
		str contents = readFile(file);
		contents = removeComments(contents);
		contents = replaceAll(contents, " ", "");
		contents = replaceAll(contents, "\r", "");
		contents = replaceAll(contents, "\t", "");
		int length = size(contents);
		while (true) {
			contents = replaceAll(contents, "\n\n", "\n");
			int newSize = size(contents);
			if (newSize == length) break;
			length = newSize;
		}
		allCode += contents;
	}
	
	tuple[real, real] percentagesDuplicateCode = calculateDuplicateCode(allCode);
	str rankByChar = rankDuplicateCodePercentage(percentagesDuplicateCode[0]);
	str rankByLoc = rankDuplicateCodePercentage(percentagesDuplicateCode[1]);
	
	println("Duplicate code rank was \"<rankByLoc>\"");
	return rankByLoc;
}

test bool getJavaFilesTest(){
	javaFiles = getJavaFiles(|project://dummy|);
	
	if (javaFiles[0] != |project://dummy/src/anothermain.java|) return false;
	if (javaFiles[1] != |project://dummy/src/main.java|) return false;

	return true;
}

test bool stripBetweenTest(){
	str stripped = "str hello; /* world */ // :^)\n ";
	
	str removeComment = stripBetween(stripped, "//", "\n");
	if (removeComment != "str hello; /* world */ ") return false;
	if (stripBetween(removeComment, "/*", "*/") != "str hello;  ") return false;
	
	return true;
	
}

test bool removeCommentsTest(){
	str stripped = "str hello; /* world */ // :^)\n ";
	
	str removeComment = removeComments(stripped);
	if (removeComment != "str hello;   ") return false;
	
	return true;	
}

test bool getRawCodeTest() {
	return getRawCode("\n\n\n\nhello\n\nworld\n") == ["hello", "world"];
}

test bool getLinesOfCodeTest() {
	return getLinesOfCode(|project://dummy|) == 10;
}

test bool rankMYbyDuplicateCodeTest() {
	if (rankMYbyDuplicateCode(|project://dummy|) != "++") return false;
	return rankMYbyDuplicateCode(|project://smallsql0.21_src|) == "o";
}


